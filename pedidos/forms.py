from django import forms
from pedidos.models import Order, Quantity
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe


class CheckboxSelectMultipleP(forms.CheckboxSelectMultiple):
    def render(self, *args, **kwargs): 
        output = super(CheckboxSelectMultipleP, self).render(*args,**kwargs) 
        return mark_safe(output.replace(u'<ul>', u'').replace(u'</ul>', u'').replace(u'<li>', u'<p>').replace(u'</li>', u'</p>'))

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ('user', 'products',)
        # widgets = {'products': CheckboxSelectMultipleP}

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('username', 'is_active', 'is_superuser', 'is_staff',
                'last_login', 'date_joined', 'id', 'groups', 'user_permissions')

class QuantityForm(forms.ModelForm):
    class Meta:
        model = Quantity
        exclude = ('order',)
