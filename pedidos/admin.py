from django.contrib import admin
from pedidos.models import Image, Color, Confectionery, Doll, Card,  Product, Order, Event, UserProfile, Category

class CategoryAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Category, CategoryAdmin)

class ImageAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Image, ImageAdmin)

class ColorAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
admin.site.register(Color, ColorAdmin)


class ConfectioneryAdmin(admin.ModelAdmin):
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
    pass
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Confectionery, ConfectioneryAdmin)

class DollAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Doll, DollAdmin)

class CardAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Card, CardAdmin)

class OrderAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Order, OrderAdmin)

class EventAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(Event, EventAdmin)

class UserProfileAdmin(admin.ModelAdmin):
    pass
#    list_display = ('id_documento', 'tipo_documento', 'fecha_desde', 'fecha_hasta', 'cliente')
#    search_fields = ('id_documento', 'tipo_documento', 'cliente')
#    list_filter = ('fecha_desde', 'fecha_hasta')
admin.site.register(UserProfile, UserProfileAdmin)

class ProductAdmin(admin.ModelAdmin):
    pass
admin.site.register(Product, ProductAdmin)
