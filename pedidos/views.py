from django.shortcuts import render_to_response
from django.template import RequestContext
from pedidos.models import Category, Product, Order
from pedidos.forms import UserForm, OrderForm, QuantityForm
from django.contrib.auth.models import User

def home(request):
    categories = Category.objects.all()
    d = dict(categories=categories)
    return render_to_response('pedidos/home.html', d,
            context_instance=RequestContext(request))

def category(request, category):
    c = Category.objects.get(name=category)
    products = Product.objects.filter(Category=c)
    d = dict(category=category, products=products)
    return render_to_response('pedidos/category.html', d,
            context_instance=RequestContext(request))

def product(request, category, slug):
    p = Product.objects.get(slug=slug)
    d = dict(Product=p)
    return render_to_response('pedidos/product.html', d,
            context_instance=RequestContext(request))

def order(request, slug):
    if request.method == 'POST':
        if request.POST.get('user_exists'):
            username = request.POST.get('username')
            oform = OrderForm(request.POST, instance=Order())
            if User.objects.filter(username=username).count():
                u = User.objects.get(username=username)
                if oform.is_valid():
                    new_order = oform.save(commit=False)
                    new_order.user = u
                    new_order.save()
        else:
            uform = UserForm(request.POST, instance=User())
            oform = OrderForm(request.POST, instance=Order())
            if oform.is_valid() and uform.is_valid():
                new_user = uform.save()
                new_order = oform.save(commit=False)
                new_order.user = new_user
                new_order.save()
    else:
        oform = OrderForm(instance=Order())
        uform = UserForm(instance=User())
        qform = QuantityForm()
        d = dict(oform=oform, uform=uform, qform=qform)
    return render_to_response('pedidos/order.html', d,
            context_instance=RequestContext(request))

