from django.db import models
from django.db.models import signals
from pedidos.signals import create_profile
from django.contrib.auth.models import User
from autoslug import AutoSlugField

import os

# When model instance is saved, trigger creation of corresponding profile
signals.post_save.connect(create_profile, sender=User)

SIZE_CHOICES = (
    ('S1', 'Size1'),
    ('S2', 'Size2'),
    ('S3', 'Size3'),
    ('S4', 'Size4'),
)
CARD_CHOICES = (
    ('S1', 'Card1'),
    ('S2', 'Card2'),
    ('S3', 'Card3'),
    ('S4', 'Card4'),
)

class Image(models.Model):
    name = models.CharField(max_length=100)
    image = models.FileField(upload_to='pedidos/static/images')

    def __unicode__(self):
        return self.name

    def get_image_url(self):
        return '/static/images/' + os.path.basename(self.image.path)


class Color(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class Confectionery(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    image = models. FileField(upload_to='images')

    def __unicode__(self):
        return self.name


class Doll(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    image = models. FileField(upload_to='images')


class Card(models.Model):
    model = models.CharField(max_length=2, choices=CARD_CHOICES)
    sender = models.CharField(max_length=50)
    recipient = models.CharField(max_length=50)
    text = models.CharField(max_length=300)


class Category(models.Model):
    name = models.CharField(max_length=100)
    image = models.FileField(upload_to='images')
    description = models.CharField(max_length=500)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    Category = models.ForeignKey(Category)
    images = models.ManyToManyField(Image)
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=5)
    dimensions = models.CharField(max_length=200)
    short_description = models.CharField(max_length=500)
    long_description = models.CharField(max_length=1000)
    color = models.ManyToManyField(Color, blank=True, null=True)
    size = models.CharField(max_length=2, choices=SIZE_CHOICES)
    slug = AutoSlugField(populate_from='name')

    def __unicode__(self):
        return self.name


class Order(models.Model):
    user = models.ForeignKey(User)
    products = models.ManyToManyField(Product, related_name='productos', through='Quantity')
    sweets = models.ForeignKey(Confectionery, blank=True, null=True)
    doll = models.ForeignKey(Doll, blank=True, null=True)
    card = models.ForeignKey(Card, blank=True, null=True)
    delivery_address = models.CharField(max_length=500)
    total_price = models.DecimalField(max_digits=10, decimal_places=5)
    delivery_date = models.DateField()
    recipient = models.CharField(max_length=100)


class Quantity(models.Model):
    order = models.ForeignKey(Order)
    product = models.ForeignKey(Product)
    quantity = models.IntegerField(default=1, verbose_name=u'cantidad')


class Event(models.Model):
    date = models.DateField()
    name = models.CharField(max_length=100)


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    surname = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    dates = models.ManyToManyField(Event, blank=True, null=True)

