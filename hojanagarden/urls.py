from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'pedidos.views.home', name='home'),
    url(r'^(?P<category>(ramos|rosas|cestas|cristal|plantas|nacimientos|bodas|coronas))/(?P<slug>[-\w]+)/$', 'pedidos.views.product', name='product'),
    url(r'^(?P<category>(ramos|rosas|cestas|cristal|plantas|nacimientos|bodas|coronas))/$', 'pedidos.views.category', name='category'),
    url(r'^order/(?P<slug>[-\w]+)/$', 'pedidos.views.order', name='order'),
    url(r'^admin/', include(admin.site.urls)),
)
